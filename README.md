# ADPointGrey conda recipe

Home: "https://github.com/areaDetector/ADPointGrey"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADPointGrey module
